# Libraries to be installed
  -keras
  -tqdm
  -opencv2

#The command for installing the libraries are present in the unet.py

#The project folder consists of -
  -codes

#All the codes are in notebook form as it was run in google colab

#################################################
Getting and preprocessing the training dataset
# run the stage_1.pynb (Images from data science bowl 2018)
# run the andrew.pynb(Breast Cancer Nuclei)
#run the BBBC.pynb(Bone marrow tissues)
#run the TNBC.pynb
# Run the final_combine.pynb


####################################################
Run the neural network
#run unet.pynb


#################################################

#############################################

The post processing process can be run by 
#run post_process.pynb
#This can also taken as a demo code for testing.
